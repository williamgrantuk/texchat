package william.grant.texchat.server;

import java.util.ArrayList;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import william.grant.texchat.common.*;

public class ChatServerHandler extends SimpleChannelInboundHandler<Message> {
    private static final ArrayList<UserData> connectedChannels = new ArrayList<>();

    /**
     * add the connecting client's channel to the connected channel list.
     */
    @Override
    public void channelActive(ChannelHandlerContext context) {
        System.out.println("A new client '" + context + "' has connected!");
        this.connectedChannels.add(new UserData("anon", context.channel()));
        System.out.println(this.connectedChannels);
    }

    /**
     * read a message from a client, sanitise it and add the html, then send to all clients
     */
    @Override
    public void channelRead0(ChannelHandlerContext context, Message message) {
        System.out.println("message:\n" + message.toString() + "\nrecieved from '" + context + "'!");

        //find the UserData corresponding to the channel
        UserData data = null;
        for (UserData connectionData : connectedChannels) {   
            if (connectionData.getChannel().equals(context.channel())) {
                data = connectionData;
                break;
            }
        }

        switch (message.getType()) {
            case ORDINARY:
                //sanitise the message
                StringBuilder messageBuilder = new StringBuilder();
                for (char c : message.getText().toCharArray()) {
                    switch (c) {
                        case '\'':
                        case '<':
                        case '>':
                        case '\"':
                        case '&':
                            messageBuilder.append('\\');  //escape naughty characters
                    }
                    messageBuilder.append(c);
                }

                //add the HTML
                messageBuilder.insert(0, "<mathjax>$$\\displaylines{\\text{" + data.getUsername() + ": } \\\\ ");
                messageBuilder.append("}$$</mathjax>");
              
                //send the message to the clients
                String out_text = messageBuilder.toString();
                for (UserData connectionData : this.connectedChannels) {
                    connectionData.getChannel().writeAndFlush(out_text);
                }
                
                break;

            case UPDATE_USERNAME:
                //update the username associated with this channel
                data.setUsername(message.getText());
                break;
        }
    }
}
