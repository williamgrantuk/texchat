package william.grant.texchat.client;

//import py4j.GatewayServer;
import py4j.ClientServer;

public class Main {
    public static void main(String[] args) {
        //GatewayServer gatewayServer = new GatewayServer(new PyEntryPoint());
        //must communicate with python using only 1 thread or netty gets upsetty
        ClientServer gatewayServer = new ClientServer(new PyEntryPoint());
        gatewayServer.startServer();
        System.out.println("gateway server started!");
    }
}

