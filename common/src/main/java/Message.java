package william.grant.texchat.common;

import java.io.Serializable;

import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ClassResolvers;

/**
 * class storing a string message and its type.
 */
public class Message implements Serializable {

    private String text;
    private MessageType type;

    public Message(String text, MessageType type) {
        this.text = text;
        this.type = type;
    }

    public String getText() {
        return this.text;
    }

    public MessageType getType() {
        return this.type;
    }

    public static ObjectDecoder getDecoder() {
        Class cls = null;
        try {
            cls = Class.forName("william.grant.texchat.common.Message");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return new ObjectDecoder(ClassResolvers.cacheDisabled(cls.getClassLoader()));
    }
}

