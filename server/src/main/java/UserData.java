package william.grant.texchat.server;

import io.netty.channel.Channel;

/**
 * class storing a client connection and corresponding username.
 */
public class UserData {
    private String username;
    private Channel channel;

    public UserData(String username, Channel channel) {
        this.username = username;
        this.channel = channel;
    }

    public Channel getChannel() {
        return this.channel;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

