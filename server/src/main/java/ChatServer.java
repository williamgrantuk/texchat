package william.grant.texchat.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
//import io.netty.handler.codec.string.StringDecoder;
//import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.codec.serialization.ClassResolvers;

import william.grant.texchat.common.Message;

public class ChatServer {
    private final int port = 6666;

    /**
     * set up and run the server.
     */
    public void run() throws InterruptedException {
        //create the event loop groups
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            //bootstrap the server
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel channel) throws Exception {
                        //add handlers to the channel pipeline
                        ChannelPipeline pipeline = channel.pipeline();
                        //pipeline.addLast(new StringDecoder());
                        //pipeline.addLast(new StringEncoder());
                        pipeline.addLast(Message.getDecoder());
                        pipeline.addLast(new ObjectEncoder());
                        pipeline.addLast(new ChatServerHandler());
                    }
                });

            //start the server
            ChannelFuture future = bootstrap.bind(port).sync();
            System.out.println("server started");

            //block until the server is closed
            future.channel().closeFuture().sync();
            System.out.println("server closed");
        } finally {
            //shutdown the event loops
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}

