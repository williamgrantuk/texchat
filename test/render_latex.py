import sys

from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtWebEngineWidgets import QWebEngineView

#the html to display
#TODO download mathjax
page_source = r"""
             <html><head>
             <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS-MML_HTMLorMML">                     
             </script></head>
             <body>
             <p>
             <mathjax>$$\displaylines{\text{william: } \\ u = \int_{-\infty}^{\infty}(awesome)\cdot du}$$</mathjax>
             </p>
             </body></html>
             """

app = QApplication(sys.argv)

window = QMainWindow()
window.setWindowTitle('look at this latex')

main_widget = QWebEngineView()
main_widget.setHtml(page_source)

window.setCentralWidget(main_widget)

window.show()
app.exec_()

