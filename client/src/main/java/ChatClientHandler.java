package william.grant.texchat.client;

import java.util.List;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ChatClientHandler extends SimpleChannelInboundHandler<String> {
    //private Outputter outputter;

    private List<String> messageList;

    //public ChatClientHandler(Outputter outputter) {
    //    this.outputter = outputter;
    //}

    public ChatClientHandler(List<String> messageList) {
        this.messageList = messageList;
    }

    @Override
    public void channelRead0(ChannelHandlerContext context, String message) {
        System.out.println("attempting to read: " + message);
        //this.outputter.output(message);
        this.messageList.add(message);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) {
        System.err.println("an error occured in the pipeline: " + cause.toString() + "\n");
        cause.printStackTrace();
        context.close();
    }
}

