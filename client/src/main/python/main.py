import sys

#from py4j.java_gateway import JavaGateway
from py4j.clientserver import ClientServer
from PyQt5.QtWidgets import QApplication

from widgets import MainWindow

if __name__ == '__main__':
    #start the gateway and retrive the client
    #gateway = JavaGateway()
    gateway = ClientServer()
    client = gateway.entry_point.getClient()

    #create the QT application
    app = QApplication(sys.argv)

    #on windows, give the process a unique AppUserModelID. This differentiates the program from the python interpreter
    try:
        from PyQt5.QtWinExtras import QtWin
    except ImportError:  #the program is running on mac/linux
        pass
    else:
        myappid = 'william.grant.texmath'
        QtWin.setCurrentProcessExplicitAppUserModelID(myappid)

    main_window = MainWindow(client)

    #run the client
    client.run()

    #run the Qt application
    main_window.show()
    app.exec_()

