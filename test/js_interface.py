import sys

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtWebChannel import QWebChannel
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEnginePage

#load the html file
#with open('js_interface.html') as f:
#    html = f.read()
#print(html)

html = r'''
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <script src="qrc:///qtwebchannel/qwebchannel.js"></script>
    <\head>
    <body> 
        <div id="chat-messages"></div>
        <script>
            var chat_messages = document.getElementById("chat-messages");

            function display_message(username, tex_string) {
                html_string = String.raw`<mathjax>$$\displaylines{\text{` + username + String.raw`: } \\ ` + tex_string + String.raw`}$$</mathjax>`;
                debug_print(html_string);
                chat_messages.insertAdjacentHTML("beforeend", html_string);
                chat_messages.scrollTop = chat_messages.scrollHeight;
            }

            new QWebChannel(qt.webChannelTransport, function (channel) {
                channel.objects.go_between.message_received.connect(display_message);
                window.debug_print = channel.objects.go_between.debug_print;
                debug_print("testtest");
            });
        <\script>
    <\body>
<\html>
'''

def sanitise(text):
    sanitised_chars = list()
    for char in text:
        if char in ("'", '<', '>', '"', '&'):
            sanitised_chars.append('\\')
        sanitised_chars.append(char)

    return ''.join(sanitised_chars)

class JsGoBetween(QObject):
    '''object exposing PyQt to javascript'''

    message_received = pyqtSignal('QString', 'QString')

    def debug_print(string):
        print("[3]", string)

class MessageDisplay(QWebEngineView):
    '''widget displaying sent messages'''

    def __init__(self):
        #initialise the widget
        super().__init__()

        #setup the local web-page
        page = QWebEnginePage(self)
        page.setHtml(html)
        self.setPage(page)

        #setup the web channel to communicate with javascript
        self.go_between = JsGoBetween()
        self.channel = QWebChannel()
        self.channel.registerObject('go_between', self.go_between)
        self.page().setWebChannel(self.channel)

    def display_message(self, username, tex_string):
        sanitised_string = sanitise(tex_string)
        print("[2]", sanitised_string)
        self.go_between.message_received.emit(sanitise(username), sanitise(tex_string))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = QMainWindow()
    window.setWindowTitle('java interface test')
    main_widget = QWidget()

    #create the display
    display = MessageDisplay()

    #setup a text box so we can enter messages
    text_box = QLineEdit()
    def on_return_pressed():
        print("[1]", text_box.text())
        display.display_message('william', text_box.text())
        text_box.setText('')
    text_box.returnPressed.connect(on_return_pressed)

    #assemble the layout
    main_layout = QVBoxLayout()
    main_layout.addWidget(display)
    main_layout.addWidget(text_box)
    main_widget.setLayout(main_layout)

    #start the application
    window.setCentralWidget(main_widget)
    window.show()
    app.exec_()

