import sys

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWebEngineWidgets import *
from apscheduler.schedulers.qt import QtScheduler

class TextSubmission(QWidget):
    def __init__(self, client, message_type, label_text):
        super().__init__()

        self.label = QLabel(label_text)

        self.line_edit = QLineEdit()

        def submit_text():
            text = self.line_edit.text()
            print("submitting text: ", text)
            client.writeMessage(text, message_type)
            self.line_edit.setText('')
        self.line_edit.returnPressed.connect(submit_text)

        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.line_edit)

        self.setLayout(layout)

class ChatDisplayWidget(QWebEngineView):
    def __init__(self):
        super().__init__()

        self.html_before = '''
<html><head>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS-MML_HTMLorMML">                     
</script></head>
<body>
<p>
'''
        self.html_after = '''
</p>
</body></html>
'''
        #self.chat_messages = list()

        self.update_display()

    def update_display(self, message_list=None):
        print("updating display!")
        if (message_list is None) or (len(message_list) == 0):
            html_body = 'no messages yet'
        else:
            html_body = '\n'.join(list(message_list))

        html = self.html_before + html_body + self.html_after
        print("got html: " + html)
        #self.setHtml(html)
        #try to set the page
        page = QWebEnginePage()
        page.setHtml(html)
        print("page html set")
        self.setPage(page)
        print("page set")
        print("html set")

        #scroll to the bottom
        self.page().runJavaScript("window.scrollTo(0,document.body.scrollHeight);")
        print("finished")

#    #implement a java interface so java classes can hold references to this class
#    class Java:
#        implements = ['william.grant.texchat.client.Outputter']

class MainWindow(QMainWindow):
    def __init__(self, client):
        super().__init__()
        self.setWindowTitle("texchat")

        self.client = client

        self.main_widget = QWidget()

        self.username_entry = TextSubmission(self.client, "UPDATE_USERNAME", "enter a username:")
        self.chat_entry = TextSubmission(self.client, "ORDINARY", "chat:")

        self.chat_display = ChatDisplayWidget()
        self.scheduler = QtScheduler()
        self.scheduler.add_job(lambda: self.chat_display.update_display(self.client.getMessageList()), 'interval', seconds=1)
        #self.scheduler.add_job(lambda: print("Tick!"), 'interval', seconds=1)
        self.scheduler.start()

        layout = QVBoxLayout()
        layout.addWidget(self.username_entry)
        layout.addWidget(self.chat_display)
        layout.addWidget(self.chat_entry)

        self.main_widget.setLayout(layout)
        self.setCentralWidget(self.main_widget)
    
