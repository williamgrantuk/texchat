package william.grant.texchat.client;

import java.util.List;
import java.util.ArrayList;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
//import io.netty.handler.codec.string.StringDecoder;
//import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.codec.serialization.ClassResolvers;

import william.grant.texchat.common.*;

public class ChatClient {
    private final String host = "127.0.0.1";
    private final int port = 6666;

    private ChannelFuture future;
    private EventLoopGroup group;

    private List<String> messageList = new ArrayList<String>();

    public void run() throws InterruptedException {
        //setup the client
        this.group = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group)
            .channel(NioSocketChannel.class)
            .handler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel channel) throws Exception {
                    ChannelPipeline pipeline = channel.pipeline();
                    //pipeline.addLast(new StringDecoder());
                    //pipeline.addLast(new StringEncoder());
                    pipeline.addLast(Message.getDecoder());
                    pipeline.addLast(new ObjectEncoder());
                    pipeline.addLast(new ChatClientHandler(messageList));
                }
            });

        //start the client
        this.future = bootstrap.connect(host, port).sync();
    }

    public void writeMessage(String message_text, String type) throws InterruptedException {
        Message message = new Message(message_text, MessageType.valueOf(type));
        Channel channel = this.future.sync().channel();
        channel.writeAndFlush(message);
        System.out.println("hopefully written: " + message_text);
    }

    public void shutdown() {
        try {
            this.future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            this.group.shutdownGracefully();
        }
    }

    public List<String> getMessageList() {
        return this.messageList;
    }
}

