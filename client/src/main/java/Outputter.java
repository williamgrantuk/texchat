package william.grant.texchat.client;

public interface Outputter {
    public void output(String message);
}

