package william.grant.texchat.server;

public class Main {
    public static void main(String[] args) {
        ChatServer server = new ChatServer();
        try {
            server.run();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}

